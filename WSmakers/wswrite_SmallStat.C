//===================================================================================================
// - Macro to prepare RooWorkspace to set an upper limit on a XS or BR or XS*BR 
//   or a signal strength of a new process with the production of a new particle.
// - Observable: a generic invariant mass of visible particles
// - Signal shape is a gaussian 
// - Background shape is a 2nd order polynomial
// - Background yield modelled in the on/off approach (this is why the macro is named "SmallStat")
//===================================================================================================

#include "RooExtendPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooPoisson.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooChebychev.h"
#include "RooPolynomial.h"
#include "RooLandau.h"
#include "RooExponential.h"
#include "RooGamma.h"
#include "RooLognormal.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"

#include "TObject.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "Riostream.h"
#include "TSystem.h"

#include "RooRandom.h"
#include "RooGlobalFunc.h"
#include "RooProdPdf.h"
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "TStopwatch.h"

#include "RooPlot.h"
#include "RooMsgService.h"
#include "RooStats/ModelConfig.h"

using namespace RooFit;
using namespace RooStats;
void wswrite_SmallStat(Int_t imass=300){
  // Define the workspace 
  RooWorkspace* w = new RooWorkspace("w");

  // Define the observable, a discriminant variable
  // In this example it is an invariant mass
  RooRealVar Minv("Minv","Minv",0,1,"GeV^{2}/c^{4}");

  // Parameter of interest:
  RooRealVar XSBRpoi("XSBRpoi","XSBRpoi",1,0,100) ;  // 

  //--------------------------------------------------------------------------------
  //------------------------ Define the background model ---------------------------
  //--------------------------------------------------------------------------------
  
  //=================================================================
  //a) Define the model for the background yield

  
  //  tau * b0 = number of expected background events in the "control region/ sidebands/..."
  //  b0 = number of expected background events in the signal region
  // see  Robert D. Cousins, James T. Linnemann, Jordan Tucker,  arXiv:physics/0702156v4 [physics.data-an], 10.1016/j.nima.2008.07.086

  
  // gb0 = global observable
  // see Kyle Cranmer, arXiv:1503.07622v1 [physics.data-an], CERN-2014-003, pp.267-308
  
  RooRealVar b0("b0","b0",50,0,100) ; // nuisance
  RooRealVar gb0("gb0","gb0",50,0,100) ; // glo
  RooRealVar b0tau("b0tau","b0tau",100,80,120) ; // nuisance

  // Constraint term:  Pois(gb0|b0tau)
  RooPoisson PoissonB0("PoissonB0","PoissonB0",gb0,b0tau);

  // All the objects have to be imported in the workspace
  w->import(PoissonB0);
  w->import(b0); 

  //=================================================================
  //b) Define the model for the background shape
  // example: second order polynomial
  
  Double_t  pa1 = 1;
  Double_t  pa2 = 10;

  // parameters of the polynomial
  RooRealVar a1("a1","a1",pa1,pa1-0.01*pa1,pa1+0.01*pa1) ;
  RooRealVar a2("a2","a2",pa2,pa2-0.01*pa2,pa2+0.01*pa2) ;

  // each "shape" parameter can be a nuisance parameter, so we need to define the constraint terms:
  // parameters for the constraint terms (Gaussian): mean (expected value for the nuisance) and sigma
  
  // global variables
  RooRealVar a1g("a1g","a1g",pa1) ;
  RooRealVar a2g("a2g","a2g",pa2) ;
  
  // sigma of the gaussian for each nuisance parameter
  RooRealVar sa1("sa1","sa1",0.01*pa1) ; // 1%
  RooRealVar sa2("sa2","sa2",0.01*pa2) ; // 1%
  
  // Constraint terms: Gaus(global obs|mean,sigma)
  RooGaussian gaus_a1("gaus_a1","gaus_a1",a1g,a1,sa1);
  RooGaussian gaus_a2("gaus_a2","gaus_a2",a2g,a2,sa2);

  
  RooPolynomial shapeBkg("shapeBkg","shapeBkg",Minv,RooArgList(a1,a2),1) ;
   
  RooAddPdf bkgPdf("bkgPdf","B-only model",RooArgList(shapeBkg),RooArgList(b0));    // We want an extended likelihood
 
  //-----------------------------------------------------------------------------------------------------
  //---------------------------------------- Define the signal model ------------------------------------
  //-----------------------------------------------------------------------------------------------------
  // It depends on the new particle mass hypothesis, in this example it is taken as input to the macro 

  
  //=======================================================================================
  // a) Define the model for the signal yield
  // nsig = (POI = parameter of interest) * (Norm = normalization factor)
  // - If the analysis searches for a new Kaon decay, the POI is a BR and Norm = 1/SES
  //   here Norm is a single nuisance parameter, but it can be the product of several parameters:
  //   SES = 1/[trigger efficiency * selection efficiency * acceptance * ... * N(K)]  
  
  Double_t normalization = 10;
  RooRealVar Norm("Norm","Norm",normalization,0.01,20) ;
  RooRealVar gNorm("gNorm","gNorm",normalization,0.01,20) ; // global variable for Norm
  
  // The constraint term for the nuisance parameter Norm is a Lognormal distribution Lognorm(gNorm|Norm,k)
  // (because Norm is a non negative multiplicative parameter)
  // The other parameter of the Lognormal is usually defined as k and is related to the
  // uncertainty associated to the parameter
  RooRealVar lnk("lnk","lnk",1.1,1.01,1.2) ; // 1.1 corresponds to a 10% uncertainty
  RooLognormal LogNormNorm("LogNormNorm","LogNormNorm",gNorm,Norm,lnk);

  //=======================================================================================
  // The parameter of interest is XSBRpoi (already defined),
  // but in the likelihood we need the expected number of signal events
  // From the "real" parameter of interest to nsig
  RooFormulaVar nsig("nsig","XSBRpoi*Norm",RooArgList(XSBRpoi,Norm));
  // ------------------------------------------------------------------
  
  //b) Define the shape of the signal in the observable: in this example it is a Gaussian with mean=mass hypothesis and sigma=0.1*mean.
  Double_t Mass = -1;
  Double_t sigmaM = -1;
  Double_t MassPoint = -1;
  Double_t MassHyp = imass;  // input
  MassPoint = imass*0.001 ;  //from MeV/c^2 to GeV/c^2
 
  Mass = MassPoint;
  sigmaM =  0.1*MassPoint ;
 
  RooRealVar mean("mean","mean of gaussian",MassPoint,max(0.001,MassPoint-0.2*MassPoint),MassPoint+0.2*MassPoint) ; 
  RooRealVar sigma("sigma","width of gaussian",sigmaM,max(0.001,sigmaM-0.1*sigmaM),0.1*sigmaM+sigmaM) ;
  RooGaussian shapeSig("shapeSig","shapeSig",Minv,mean,sigma);

  RooRealVar gmean("gmean","gmean",MassPoint,max(0.001,MassPoint-0.1*MassPoint),MassPoint+0.1*MassPoint) ;  // Global for mean
  RooRealVar gsigma("gsigma","width of gaussian",sigmaM,max(0.001,sigmaM-0.1*sigmaM),sigmaM+sigmaM) ;  // Global for sigma
  RooRealVar lnksig("lnksig","lnksig",1.1,1.01,1.5) ;  //  k=1.1  ==> 10% of uncertainty for sigma

  // Sigma is a non-negative variable, Lognormal distribution is an appropriate constraint
  RooLognormal LogNormSigma("LogNormSigma","LogNormSigma",gsigma,sigma,lnksig);
  
  RooRealVar sigmamean("sigmamean","sigmamean",MassPoint*0.02,0.001,MassPoint*0.1) ;  // sigma of the Gaussian constraint for the nuisance "mean"
  RooLognormal GausMean("GausMean","GausMean",gmean,mean,sigmamean); // Gaussian constraint for mean 
  
  //----------------
  
  RooAddPdf modelMean("modelMean","modelMean",RooArgList(bkgPdf,shapeSig),RooArgList(b0,nsig)) ;  
  
  RooProdPdf model("model","model",RooArgSet(modelMean				 
					     ,PoissonB0 // Constraint for the nuisance b0
					     ,LogNormNorm // Constraint for the nuisance Norm
					     ,LogNormSigma //
					     ,GausMean //
					     ,gaus_a1
					     ,gaus_a2
					     )
		   );
					     
  char massname[150] = "";
  sprintf(massname,"m_{hyp}= %d MeV",int(MassHyp));


  // Create the RooPlot 
  RooPlot* frame = Minv.frame(Name("frame"),Title("Model"),Bins(20)) ;
  TCanvas* cb = new TCanvas("cb","cb",1000,700) ;
  cb->Divide(1,2);
  cb->cd(1);
  cb->SetMargin(0.15,0.15,0.15,0.15);
 
  frame->SetTitleSize(0.05,"xy");
  //frame->SetYTitle("a.u.");
  frame->SetXTitle("m_{inv} [GeV/c^{2}]");
  frame->SetTitle(massname);
  //  bkgPdf.plotOn(frame,LineColor(kBlue)) ;
  //  model.plotOn(frame,LineColor(kViolet)) ;
  // shapeSig.plotOn(frame,LineColor(kRed)) ;

  w->import(Minv);
  w->import(nsig);
  w->import(shapeSig);
  w->import(bkgPdf);

  w->import(LogNormNorm);
  w->import(LogNormSigma);
  w->import(GausMean);
  w->import(gaus_a1);
  w->import(gaus_a2);
  
  w->defineSet("obs","Minv");
  w->defineSet("poi","XSBRpoi");

  w->defineSet("nuisParams","b0,sigma,mean,Norm,a1,a2");
  w->defineSet("global", "gb0,gmean,gsigma,gNorm,lnk,lnksig,a1g,a2g");
  //=====================================================================
  // All the global observables must be set constant !!!!
  //=====================================================================
  // They are constant in the fit for the PLL but they vary in the toys
  
   
  w->var("gb0")->setConstant(true);  //
  w->var("gNorm")->setConstant(true);
  w->var("a1g")->setConstant(true);
  w->var("a2g")->setConstant(true);
  w->var("gmean")->setConstant(true);
  w->var("gsigma")->setConstant(true);
  
  w->var("lnk")->setConstant(true);
  w->var("lnksig")->setConstant(true);


  //=============  FAKE DATA!!!! =====================
  // First we generate them with the bkg distribution
  //==================================================
 
  RooRandom::randomGenerator()->SetSeed(1234); // I want that the data are the same for each mass hypothesis
  RooDataSet * fakeDataB = bkgPdf.generate(Minv,50);  // only-bkg hypothesys
  fakeDataB->SetName("fakeDataB");
  //==================================================

 // FAKE DATA with SIGNAL
  // Now we generate a dataset based on the hypothesis that there is a signal with an invariant mass m=200 MeV
  RooRealVar FakeMean("FakeMean","mean of gaussian",0.2) ; // M=200 MeV 
  RooRealVar FakeSigma("FakeSigma","width of gaussian",0.02) ;  //
  RooGaussian shapeFakeSig("shapeFakeSig","shapeFakeSig",Minv,FakeMean,FakeSigma);
  RooRealVar nFakeSig("nFakeSig","n of sig",10,0,20) ;
  
  RooAddPdf FakeModel("FakeSigPdf","Fake SIG model",RooArgList(bkgPdf,shapeFakeSig),RooArgList(b0,nFakeSig));
  RooDataSet *fakeDataSB = FakeModel.generate(Minv,55);  
  fakeDataSB->SetName("fakeDataSB");
  w->import(*fakeDataSB);
  w->import(*fakeDataB);
  bkgPdf.plotOn(frame,LineColor(kBlue)) ;
  model.plotOn(frame,LineColor(kViolet)) ;
  //fakeDataSB->plotOn(frame,MarkerColor(kRed)) ;
  //fakeDataB->plotOn(frame,MarkerColor(1)) ;
  
  frame->Draw();

  RooPlot* frameD = Minv.frame(Name("frameD"),Title("Dataset"),Bins(20)) ;
  //TCanvas* cb = new TCanvas("cb","cb",1000,700) ;
  cb->cd(2);
  //  cb->SetMargin(0.2,0.08,0.2,0.08);
  frameD->SetTitleSize(0.05,"xy");
  // frame->SetYTitle("a.u.");
  frameD->SetXTitle("m_{inv} [GeV/c^{2}]");
  fakeDataSB->plotOn(frameD,MarkerColor(kRed)) ;
  fakeDataB->plotOn(frameD,MarkerColor(1)) ;
  
  frameD->Draw();

  
  TString outdir = "../WS_SmallStats/";
  gSystem->Exec("mkdir "+outdir);
  char plotname[150] = "";
  sprintf(plotname,"Fake_m%d.pdf",int(MassHyp));
  cb->SaveAs(outdir+TString(plotname));


  ModelConfig b_model("B_model", w);
  b_model.SetPdf(model);
  b_model.SetObservables(*w->set("obs"));
  b_model.SetParametersOfInterest(*w->set("poi"));
  b_model.SetNuisanceParameters(*w->set("nuisParams"));
  b_model.SetGlobalObservables(*w->set("global"));
  w->var("XSBRpoi")->setVal(0.0); 
  b_model.SetSnapshot(*w->set("poi"));

  ModelConfig sb_model("SB_model", w);
  sb_model.SetPdf(model);
  sb_model.SetObservables(*w->set("obs"));
  sb_model.SetParametersOfInterest(*w->set("poi"));
  w->var("XSBRpoi")->setVal(1);
  sb_model.SetSnapshot(*w->set("poi"));
  sb_model.SetNuisanceParameters(*w->set("nuisParams"));
  sb_model.SetConstraintParameters(*w->set("nuisParams"));
  sb_model.SetGlobalObservables(*w->set("global"));
  w->import(sb_model) ;
  w->import(b_model) ;
  w->Print() ;
  
  char wsname[150] = "";
  sprintf(wsname,"Fake_ws_m%d.root",int(MassHyp));
  
  TString wsfilename(outdir+TString(wsname));
  w->writeToFile(wsfilename) ;  
  gDirectory->Add(w) ;
  return;
}
